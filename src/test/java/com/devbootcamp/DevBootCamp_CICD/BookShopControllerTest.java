package com.devbootcamp.DevBootCamp_CICD;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class BookShopControllerTest {

    private BookShopController bookShopController;
    private BookShopDTO bookShopDTO;

    @Before
    public void initializeController() {
        bookShopController = new BookShopController();
        bookShopDTO = new BookShopDTO("CLASSMATE") ;
    }

    @Test
    public void testBookShopControllerInstance(){
        Assert.assertNotNull(bookShopController);
    }
    @Test
    public void  shouldReturnBookShopObject() {
     Assert.assertEquals(bookShopDTO,bookShopController.getBookShops("CLASSMATE"));

    }

}
