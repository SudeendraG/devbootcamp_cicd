package com.devbootcamp.DevBootCamp_CICD;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevBootCampCicdApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevBootCampCicdApplication.class, args);
	}

}
