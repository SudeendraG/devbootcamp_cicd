package com.devbootcamp.DevBootCamp_CICD;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookShopController {

    @GetMapping("/application")
    public BookShopDTO getBookShops(@RequestParam("title") String bookTitle) {

        return new BookShopDTO(bookTitle);
    }
}
