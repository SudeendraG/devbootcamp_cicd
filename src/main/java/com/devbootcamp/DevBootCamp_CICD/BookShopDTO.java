package com.devbootcamp.DevBootCamp_CICD;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BookShopDTO {
    String title;
}
